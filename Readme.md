# Shuajo.eu

This is a website I created to share moments and small games with friends and family. You can look at it live at https://shuajo.eu/.

## Notes

Feel free to clone the repository to look at it offline. Note that I have removed the passwords from the application.properties files. 
If you want to run it fully you need some form of database (I used PostgreSQL), create the tables and insert the necessary data into it.
Testing should run fine though, as it uses an in-memory-database and creates all the necessities on its own.

If you want to build it, you can use the provided gradle wrapper or install gradle yourself.

To use the gradle wrapper, go to the root directory and type:
```
./gradlew build
```

To install gradle, please take a look here: https://gradle.org/install.