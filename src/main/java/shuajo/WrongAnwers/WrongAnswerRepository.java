package shuajo.WrongAnwers;

import org.springframework.data.repository.CrudRepository;
import shuajo.MessageBoard.MessageBoardPost;
import shuajo.Solutions.Solution;

import java.util.List;

/**
 * Created by joshua on 5/25/17.
 */
public interface WrongAnswerRepository extends CrudRepository<WrongAnswer, Long> {
    List<WrongAnswer> findByOriginOrderByTimeEnteredDesc(String origin);
    List<WrongAnswer> findAllByOrderByTimeEnteredDescOriginAsc();

}
