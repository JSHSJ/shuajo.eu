package shuajo.WrongAnwers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shuajo.ExceptionHandling.ExceptionHandler;
import shuajo.Helper.TimeHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joshua on 5/25/17.
 */
@Service
public class WrongAnswerDatabaseHelper {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    @Autowired
    WrongAnswerRepository wrongAnswerRepository;

    @Autowired
    TimeHelper timeHelper;

    private String origin;
    private String answer;
    private WrongAnswer wrongAnswer;

    public void createNewWrongAnswerFromException(RuntimeException e) {

        this.wrongAnswer = new WrongAnswer();
        String message = e.getMessage();
        parseExceptionMessage(message);
        this.wrongAnswer.setTimeEntered(timeHelper.calculateCurrentTime());
        this.wrongAnswerRepository.save(wrongAnswer);


    }

    private void parseExceptionMessage(String message) {
        try {
            String[] words = message.split("\"");
            System.out.println(words[0].trim());
            System.out.println(words[1]);
            this.wrongAnswer.setOrigin(words[0].trim());
            this.wrongAnswer.setAnswer(words[1]);

        } catch(Exception ex) {
            logger.info("Exception parsing error for WrongAnswer");
            logger.info(ex.getMessage());
        }

    }

    public List<String> getWrongAnswerNamesByOrigin(String origin) {
        List<WrongAnswer> answers = this.wrongAnswerRepository.findByOriginOrderByTimeEnteredDesc(origin);
        ArrayList<String> answersAsStrings = new ArrayList<>();
        for (WrongAnswer a : answers) {
            answersAsStrings.add(a.getAnswer());
        }
        return answersAsStrings;
    }

    public List<WrongAnswer> getWrongSolutions() {
        return this.wrongAnswerRepository.findByOriginOrderByTimeEnteredDesc("Solution");
    }

    public List<WrongAnswer> getWrongEntries() {
        return this.wrongAnswerRepository.findByOriginOrderByTimeEnteredDesc("Entry");
    }

    public List<WrongAnswer> getAllWrongAnswers() {
        return this.wrongAnswerRepository.findAllByOrderByTimeEnteredDescOriginAsc();
    }
}
