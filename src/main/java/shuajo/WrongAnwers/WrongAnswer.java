package shuajo.WrongAnwers;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by joshua on 5/25/17.
 */
@Entity
@Table(name = "wrong_answers")
public class WrongAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wrong_answers_seq_gen")
    @SequenceGenerator(name = "wrong_answers_seq_gen", sequenceName = "wrong_answers_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    private String answer;
    private String origin;
    private Date timeEntered;

    public WrongAnswer() {
    }

    public String getAnswer() {
        return answer;
    }

    public Date getTimeEntered() {
        return timeEntered;
    }

    public String getOrigin() {
        return this.origin;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setTimeEntered(Date timeEntered) {
        this.timeEntered = timeEntered;
    }
}
