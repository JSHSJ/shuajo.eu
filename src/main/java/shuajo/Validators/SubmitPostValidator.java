package shuajo.Validators;

import org.springframework.validation.ValidationUtils;
import shuajo.MessageBoard.MessageBoardPost;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by joshua on 5/10/17.
 */
public class SubmitPostValidator implements Validator {
    private static String emptyString = "";
    private String link;
    private String message;
    private String poster;


    @Override
    public boolean supports(Class<?> clazz) {
        return MessageBoardPost.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MessageBoardPost post = (MessageBoardPost) target;
        this.poster = post.getPoster();
        this.message = post.getMessage();
        this.link = post.getLink();
        if (isPosterNameWrongLength()) {
            errors.rejectValue("poster", "poster.wrongLength");
        }
        if (areBothFieldsOfPostEmpty()) {
            errors.rejectValue("message", "message.emptyPost");
        }
        if (isMessageTooLong()) {
            errors.rejectValue("message", "message.tooLong");
        }
    }

    private boolean areBothFieldsOfPostEmpty() {
        link = link.trim();
        message = message.trim();
        return (emptyString.equals(link) && emptyString.equals(message));
    }

    private boolean isMessageTooLong() {
        return this.message.length() > 200;

    }

    private boolean isPosterNameWrongLength() {
        return (this.poster.length() < 3 || this.poster.length() > 10);
    }
}
