package shuajo.Entries;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by joshua on 5/7/17.
 */
@Repository
public interface EntryRepository extends CrudRepository<Entry, Long> {
    List<Entry> findByEntryName(String entryName);
}