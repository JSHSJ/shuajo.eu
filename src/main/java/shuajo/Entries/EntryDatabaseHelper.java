package shuajo.Entries;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shuajo.ExceptionHandling.Exceptions.EntryNotFoundException;

import java.util.List;

/**
 * Created by joshua on 5/7/17.
 */
@Service
public class EntryDatabaseHelper {

    @Autowired
    private EntryRepository entryRepository;

    private List<Entry> currentEntries;

    public String getEntryByName(String name) {
        currentEntries = this.entryRepository.findByEntryName(name);
        if (!entryExists()) {
            throw new EntryNotFoundException(name);
        } else {
            return currentEntries.get(0).getMapping();
        }
    }


    private boolean entryExists() {
        return !currentEntries.isEmpty();
    }
}
