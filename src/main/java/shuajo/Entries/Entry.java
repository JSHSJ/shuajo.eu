package shuajo.Entries;

import javax.persistence.*;

/**
 * Created by joshua on 5/7/17.
 */
@Entity
@Table(name = "entries")
public class Entry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private String entryName;
    private String entryMapping;

    protected Entry(){
    }


    public String getEntryName() {
        return this.entryName;
    }

    public String getMapping() {
        return this.entryMapping;
    }

}
