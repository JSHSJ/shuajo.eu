package shuajo.Helper;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by joshua on 5/25/17.
 */
@org.springframework.stereotype.Service
public class TimeHelper {

    public Date calculateCurrentTime() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        return new Date(cal.getTime().getTime());
    }
}
