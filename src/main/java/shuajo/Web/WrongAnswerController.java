package shuajo.Web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import shuajo.WrongAnwers.WrongAnswer;
import shuajo.WrongAnwers.WrongAnswerDatabaseHelper;

import java.util.List;

/**
 * Created by joshua on 5/25/17.
 */
@Controller
public class WrongAnswerController {

    private ModelManager modelManager = new ModelManager();

    @Autowired
    WrongAnswerDatabaseHelper wrongAnswerDatabaseHelper;

    @GetMapping("/wrongAnswers")
    public String showAllWrongAnswers(Model model) {
        List<WrongAnswer> allAnswers = wrongAnswerDatabaseHelper.getAllWrongAnswers();
        model.addAttribute("answers", allAnswers);
        model.addAttribute("allAnswers", "all");
        modelManager.setModel(model);
        return modelManager.processWrongAnswers();
    }

    @GetMapping("/wrongAnswers/{origin}")
    public String showAnswersByOrigin(@PathVariable("origin") String origin, Model model) {
        List<String> allAnswers = wrongAnswerDatabaseHelper.getWrongAnswerNamesByOrigin(origin);
        model.addAttribute("answers", allAnswers);
        model.addAttribute("allAnswers", origin);
        modelManager.setModel(model);
        return modelManager.processWrongAnswers();
    }
}
