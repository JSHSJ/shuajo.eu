package shuajo.Web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shuajo.Entries.EntryDatabaseHelper;

/**
 * Created by joshua on 5/7/17.
 */
@Controller
public class EntryController {

    private ModelManager modelManager = new ModelManager();

    @Autowired
    private EntryDatabaseHelper entryDatabaseHelper;

    @GetMapping("/entry")
    public String showLevelLogin(Model model) {
        modelManager.setModel(model);
        return modelManager.processLevelLogin();
    }

    @PostMapping("/entry")
    public String checkForLevel(@RequestParam("entryName") String entryName, Model model) {
        String mapping = this.entryDatabaseHelper.getEntryByName(entryName);
        return "redirect:/entry/"+mapping;
    }

    @GetMapping("/entry/{entryMapping}")
    public String getLevel(@PathVariable("entryMapping") String entryMapping, Model model) {
        model.addAttribute("entry", entryMapping);
        modelManager.setModel(model);
        return modelManager.processEntry();
    }
}
