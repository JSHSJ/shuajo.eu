package shuajo.Web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by joshua on 5/7/17.
 */
@Controller
public class MainController {

    private ModelManager modelManager = new ModelManager();

    @GetMapping("/")
    public String showHome(Model model) {
        modelManager.setModel(model);
        return modelManager.processHome();
    }

}
