package shuajo.Web;

/**
 * Created by joshua on 5/9/17.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shuajo.Solutions.SolutionDatabaseHelper;
@Controller
public class SolutionController {

    private ModelManager modelManager = new ModelManager();

    @Autowired
    private SolutionDatabaseHelper solutionDatabaseHelper;

    @GetMapping("/solution")
    public String showSolutionForm(Model model) {
        modelManager.setModel(model);
        return modelManager.processSolutionForm();
    }

    @PostMapping("/solution")
    public String checkForLevel(@RequestParam("solutionName") String solutionName, Model model) {
        String mapping = this.solutionDatabaseHelper.getSolutionByName(solutionName);
        return "redirect:/solution/"+mapping;
    }

    @GetMapping("/solution/{solutionMapping}")
    public String getLevel(@PathVariable("solutionMapping") String solutionMapping, Model model) {
        model.addAttribute("solution", solutionMapping);
        modelManager.setModel(model);
        return modelManager.processSolution();
    }
}
