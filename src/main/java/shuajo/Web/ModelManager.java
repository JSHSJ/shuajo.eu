package shuajo.Web;

import org.springframework.ui.Model;

/**
 * Created by joshua on 5/7/17.
 */
public class ModelManager {

    private Model model;

    public ModelManager() {}

    public void setModel(Model model) {
        this.model = model;
    }

    public String processHome() {
        model.addAttribute("title", "home.title");
        model.addAttribute("templateType", "DefaultTemplate");
        model.addAttribute("content", "home");
        model.addAttribute("reqJQuery", false);
        model.addAttribute("reqNavigation", false);
        return "Layout";
    }

    public String processLevelLogin() {
        model.addAttribute("title", "level-login.title");
        model.addAttribute("templateType", "DefaultTemplate");
        model.addAttribute("content", "level-login");
        model.addAttribute("reqJQuery", false);
        model.addAttribute("reqNavigation", true);
        return "Layout";
    }

    public String processError() {
        model.addAttribute("title", "error.title");
        model.addAttribute("templateType", "DefaultTemplate");
        model.addAttribute("content", "error");
        model.addAttribute("reqJQuery", false);
        model.addAttribute("reqNavigation", true);
        return "Layout";
    }

    public String processEntry() {
        model.addAttribute("title", model.asMap().get("entry")+".title");
        model.addAttribute("templateType", "NoTemplate");
        model.addAttribute("content", "entry");
        model.addAttribute("reqJQuery", true);
        model.addAttribute("reqNavigation", true);
        return "Layout";
    }

    public String processSolutionForm() {
        model.addAttribute("title", "solution-form.title");
        model.addAttribute("templateType", "DefaultTemplate");
        model.addAttribute("content", "solution-form");
        model.addAttribute("reqJQuery", false);
        model.addAttribute("reqNavigation", true);
        return "Layout";
    }

    public String processSolution() {
        model.addAttribute("title", model.asMap().get("solution")+".title");
        model.addAttribute("templateType", "NoTemplate");
        model.addAttribute("content", "solution");
        model.addAttribute("reqJQuery", true);
        model.addAttribute("reqNavigation", true);
        return "Layout";
    }

    public String processMessageBoardIndex() {
        model.addAttribute("title", "messageBoardIndex.title");
        model.addAttribute("templateType", "DefaultTemplate");
        model.addAttribute("content", "messageBoardIndex");
        model.addAttribute("reqJQuery", true);
        model.addAttribute("reqNavigation", true);
        return "Layout";
    }

    public String processWrongAnswers() {
        model.addAttribute("title", "wrongAnswers.title");
        model.addAttribute("templateType", "DefaultTemplate");
        model.addAttribute("content", "wrongAnswers");
        model.addAttribute("reqJQuery", true);
        model.addAttribute("reqNavigation", true);
        return "Layout";
    }

}
