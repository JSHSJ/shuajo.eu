package shuajo.Web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import shuajo.ExceptionHandling.ExceptionHandler;
import shuajo.MessageBoard.MessageBoardHelper;
import shuajo.MessageBoard.MessageBoardPost;
import shuajo.Validators.SubmitPostValidator;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by joshua on 5/10/17.
 */
@Controller
public class MessageBoardController {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    @Autowired
    private MessageBoardHelper messageBoardHelper;


    private ModelManager modelManager = new ModelManager();

    @GetMapping("/messageBoard")
    public String getMessageBoardIndex(Model model) {
        model = addNewPostAndAllPostsToModel(model);
        modelManager.setModel(model);
        return modelManager.processMessageBoardIndex();
    }

    @PostMapping("/messageBoard")
    public String addSomethingToMessageBoard(@ModelAttribute("messageBoardPost") @Valid MessageBoardPost messageBoardPost,
                                        BindingResult result,
                                        Model model) {

        this.messageBoardHelper.setCurrentPost(messageBoardPost);
        ValidationUtils.invokeValidator(new SubmitPostValidator(),messageBoardPost, result);

        if (result.hasErrors()) {
            return postWithError(model);
        }

        this.messageBoardHelper.saveNewPost();
        model = addNewPostAndAllPostsToModel(model);
        modelManager.setModel(model);
        return modelManager.processMessageBoardIndex();
    }

    private String postWithError(Model model) {
        modelManager.setModel(model);
        return modelManager.processMessageBoardIndex();
    }

    private Model addNewPostAndAllPostsToModel(Model model) {
        List<MessageBoardPost> messageBoardPosts = this.messageBoardHelper.getMessageBoardPosts();
        MessageBoardPost messageBoardPost = this.messageBoardHelper.getNewMessagePost();
        model.addAttribute("messageBoardPosts", messageBoardPosts);
        model.addAttribute("messageBoardPost", messageBoardPost);
        return model;
    }
}
