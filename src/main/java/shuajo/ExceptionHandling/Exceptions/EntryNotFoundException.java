package shuajo.ExceptionHandling.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by joshua on 5/7/17.
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class EntryNotFoundException extends RuntimeException{

    private String answer;

    public EntryNotFoundException(String answer) {
        super("Entry \"" + answer + "\" could not be found!");
        this.answer = answer;
    }

    public String getAnswer() {
        return this.answer;
    }
}
