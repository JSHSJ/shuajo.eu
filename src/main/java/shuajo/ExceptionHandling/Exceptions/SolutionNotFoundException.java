package shuajo.ExceptionHandling.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by joshua on 5/9/17.
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class SolutionNotFoundException extends RuntimeException{

    private String answer;

    public SolutionNotFoundException(String answer) {
        super("Solution \"" + answer + "\" could not be found!");
        this.answer = answer;
    }

    public String getAnswer() {
        return this.answer;
    }
}
