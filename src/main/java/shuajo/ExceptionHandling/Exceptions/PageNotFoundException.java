package shuajo.ExceptionHandling.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by joshua on 5/24/17.
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class PageNotFoundException extends RuntimeException{

    private String path;

    public PageNotFoundException(String path) {
        super("The requested page at " + path + " doesn't exist!");
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
