package shuajo.ExceptionHandling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import shuajo.Web.ModelManager;

/**
 * Created by joshua on 5/24/17.
 */
@Controller
public class FallbackErrorController implements ErrorController {

    private static final String PATH = "/error";
    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    private ModelManager modelManager = new ModelManager();


    /**
     *
     * @param model
     * @param e gets exception
     * @return returns general error page with path
     */
    @RequestMapping(value = PATH)
    public String handleError(Model model, Exception e) {
        logger.info("MappingNotFound: " + e.toString());
        model.addAttribute("error", "Something's wrong!");
        modelManager.setModel(model);
        return modelManager.processError();
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
