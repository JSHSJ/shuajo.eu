package shuajo.ExceptionHandling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;
import shuajo.ExceptionHandling.Exceptions.EntryNotFoundException;
import shuajo.ExceptionHandling.Exceptions.SolutionNotFoundException;
import shuajo.Web.ModelManager;
import shuajo.WrongAnwers.WrongAnswerDatabaseHelper;

/**
 * Created by joshua on 5/7/17.
 */
@ControllerAdvice
public class ExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    private ModelManager modelManager = new ModelManager();

    @Autowired
    WrongAnswerDatabaseHelper wrongAnswerDatabaseHelper;

    @org.springframework.web.bind.annotation.ExceptionHandler({EntryNotFoundException.class, SolutionNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleNotFoundException(RuntimeException exception, Model model) {
        logger.info("CustomNotFoundException: " + exception.toString());
        wrongAnswerDatabaseHelper.createNewWrongAnswerFromException(exception);
        model.addAttribute("error", exception.getMessage());
        modelManager.setModel(model);
        return modelManager.processError();
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleHandlerNotFound(NoHandlerFoundException exception, Model model) {
        logger.info("HandlerNotFoundException: " + exception.getRequestURL());
        model.addAttribute("error", "The path " + exception.getRequestURL() + " doesn't seem to exist!");
        modelManager.setModel(model);
        return modelManager.processError();
    }
}
