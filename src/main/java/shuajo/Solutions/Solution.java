package shuajo.Solutions;

import javax.persistence.*;

/**
 * Created by joshua on 5/7/17.
 */
@Entity
@Table(name = "solutions")
public class Solution {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private String solutionName;
    private String solutionMapping;

    protected Solution(){
    }

    public void setSolutionName(String solutionName) {
        this.solutionName = solutionName;
    }

    public void setSolutionMapping(String solutionMapping) {
        this.solutionMapping = solutionMapping;
    }

    public String getSolutionName() {
        return this.solutionName;
    }

    public String getMapping() {
        return this.solutionMapping;
    }

}
