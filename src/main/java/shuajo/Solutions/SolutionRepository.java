package shuajo.Solutions;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by joshua on 5/9/17.
 */
public interface SolutionRepository extends CrudRepository<Solution, Long> {
    List<Solution> findBySolutionName(String solutionName);

}
