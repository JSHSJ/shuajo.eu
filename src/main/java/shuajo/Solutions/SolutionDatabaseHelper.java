package shuajo.Solutions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import shuajo.ExceptionHandling.Exceptions.SolutionNotFoundException;

import java.util.List;

/**
 * Created by joshua on 5/9/17.
 */
@Service
public class SolutionDatabaseHelper {

    @Autowired
    private SolutionRepository solutionRepository;

    private List<Solution> matchedSolutions;

    public String getSolutionByName(String name) {
        matchedSolutions = this.solutionRepository.findBySolutionName(name);
        if (!solutionExists()) {
            throw new SolutionNotFoundException(name);
        } else {
            return matchedSolutions.get(0).getMapping();
        }
    }


    private boolean solutionExists() {
        return !matchedSolutions.isEmpty();
    }
}

