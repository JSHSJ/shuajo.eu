package shuajo.MessageBoard;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import shuajo.Helper.TimeHelper;
import shuajo.Validators.SubmitPostValidator;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by joshua on 5/10/17.
 */

@Service
public class MessageBoardHelper {

    @Autowired
    private MessageBoardPostRepository messageBoardPostRepository;

    @Autowired
    private TimeHelper timeHelper;

    private MessageBoardPost currentPost;

    private List<MessageBoardPost> messageBoardPosts;


    public MessageBoardPost getNewMessagePost() {
        return new MessageBoardPost();
    }

    public void saveNewPost() {
        this.currentPost.setTimePosted(timeHelper.calculateCurrentTime());
        this.messageBoardPostRepository.save(this.currentPost);
        updateMessageBoard();
    }


    public void setCurrentPost(MessageBoardPost post) {
        this.currentPost = post;
    }

    public List<MessageBoardPost> getMessageBoardPosts() {
        return this.messageBoardPosts;
    }

    private void updateMessageBoard() {
        this.messageBoardPosts = this.messageBoardPostRepository.findAllByOrderByTimePostedDesc();
    }

    // Initialise leaderboard on App Startup
    @PostConstruct
    private void setMessageBoardPostsOnStartup() {
        updateMessageBoard();
    }


}
