package shuajo.MessageBoard;

import org.springframework.data.repository.CrudRepository;
import shuajo.Solutions.Solution;

import java.util.List;

/**
 * Created by joshua on 5/10/17.
 */
public interface MessageBoardPostRepository extends CrudRepository<MessageBoardPost, Long> {
    List<MessageBoardPost> findAllByOrderByTimePostedDesc();
}
