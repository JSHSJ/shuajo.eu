package shuajo.MessageBoard;

import org.springframework.beans.factory.annotation.Autowired;
import shuajo.Helper.TimeHelper;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by joshua on 5/10/17.
 */
@Entity
@Table(name = "message_board_posts")
public class MessageBoardPost {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "message_board_posts_seq_gen")
    @SequenceGenerator(name = "message_board_posts_seq_gen", sequenceName = "message_board_posts_id_seq", allocationSize = 1)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    private String message = "";
    private String link = "";
    private String poster = "";

    private Date timePosted;

    public MessageBoardPost() {
    }
    public String getContent() {
        String properContent = makeProperContent();
        return properContent;
    }

    private String makeProperContent() {
        if (this.link.equals("")) {
            return this.message;
        } else {
            return convertLinkToHTML();
        }
    }


    private String convertLinkToHTML() {
        if ("".equals(message.trim())) {
            this.message = link;
        }
       String correctLink = "<a href=\""+this.link+"\" target=\"_blank\">"
                +message+"</a>";
       return correctLink;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public void setTimePosted(Date timePosted) {
        this.timePosted = timePosted;
    }

    public String getPoster() {
        return this.poster;
    }

    public Date getTimePosted() {
        return timePosted;
    }
    public String getMessage() {
        return message;
    }
    public String getLink() {
        return link;
    }

}
