/**
 * Created by joshua on 5/30/17.
 */

$(document).ready(function () {
    var counter = 0;
    var answers = ["hase","kiefer", "tajine", "schwerbelastungskörper","schlachtensee","24", "natur"];
    var questions = ["Sehr gut . Das ist nur eine Übung! Klickt auf ' T ipp' für das Lösungswort.",
        "Ist das kein schöner Wald? Ganz typ i sch für Berlin! Aber wie nennt man diese Bäume?",
        "Das war lecker. Doch wie heißt nochmal dieses Kochbehältnis?",
        "Ok, das ist schwieri g er, aber auch sehr spannend! Ein Stück Berliner Geschichte, relativ unbekannt. " +
        "Es handelt sich um einen gigantischen Klotz aus Zement, der zu Nazi-Zeiten gebaut wurde.",
        "Ein wunderschöner See bei mir um die Ecke. Oh, da gibt's ja mehrere... Dieser ist ein weni g  weiter südlich," +
        " liegt zwischen der A115 und den S-Bahn-Gleisen und hat eine S-Bahn-Station mit seinem Namen.",
        "Das ist mal eine Wurzel... Achja. Was ist die Wurz e l aus 576?",
        "Ich hoffe, ihr habt noch gute Augen und einen kla r en Kopf für dieses kleine Rätsel. Kleine Buchstaben wollen " +
        "zusammen gesetzt werden."];
    var hints = ["Das Lösungswort ist 'Hase'",
        "Das gleiche Wort beschreibt auch den unteren Teil des Gesichts.",
        "Es handelt sich um ein nordafrikanisches Schmorgefäß mit spitzem, konischem Deckel.",
        "Zieht euch Google zu Rate. Sucht nach Schlagworten!",
        "Der Name besteht aus 2 Worten: 1. Ein Teil von Krieg, 2. Eine Art von Gewässer",
        "Das kriegt ihr auch so hin... 2 * 3 * 4",
        "Zufällig ist der Großbegriff für das was ihr hier seht das gesuchte Wort. 5 Buchstaben, einer ist groß zur Hilfe"];

    var imageHidden = false;
    var widthToScroll = $("#slide0").width() + 5;
    updateTexts();

    //prevent scrolling
    $("body").scroll(function(e){ e.preventDefault()});

    function updateTexts () {
        $(".question-text").text(questions[counter]);
        $(".question-hint").text(hints[counter]);
    };

    $(".image-border").click(function () {
        $(".question-box").fadeToggle(200);
        $("#answerField").focus();
        toggleImageOpacity();
    });

    function toggleImageOpacity() {
        if (!imageHidden) {
            $("img").css("opacity", 0.6);
        } else {
            $("img").css("opacity", 1);
        }
        imageHidden = !imageHidden;
    }

    $("#submit").click(function () {
        if(!checkCorrectAnswer()) {
            flashWindow();
        } else if(isOver()) {
            displayEndscreen();
        } else {
            advanceLevel();
        }
    });

    function advanceLevel() {
        counter++;
        $(".img-container").animate({scrollLeft: "+=" + widthToScroll}, 1000);
        $(".question-box").fadeToggle(200);
        $("#answerField").val("");
        $(".question-hint").hide();
        updateTexts();
        $("img").css("opacity", 1);
        imageHidden = false;
    }

    function checkCorrectAnswer() {
        var answerToTest = $("#answerField").val().toLowerCase();
        return answerToTest===answers[counter];
    }


    function isOver() {
        return counter >= questions.length-1;
    }

    function displayEndscreen() {
        toggleImageOpacity();
        $(".img-container").animate({scrollLeft: "-=" + (widthToScroll*6)}, 2000);
        populateEndscreen();
        counter = 0;
        updateTexts();
        $(".question-box").fadeToggle(200);
    }

    function populateEndscreen() {
        $(".image-slide-description").text("");
        $(".image-slide-description").append("<h1 class='headline'>Sehr gut!</h1>");
        $(".image-slide-description").append("<p>Gut Gemacht! Habt ihr gut aufgepasst und ein weiteres Lösungswort" +
            " gefunden? Dann klickt auf den folgenden Link und gebt es dort ein!</p>");
        var newLink = $("<a />", {
            name : "Zur Lösung",
            href : "/solution",
            text : "Zur Lösung",
            target: "_blank"
        });
        $(".image-slide-description").append(newLink);
        $(".image-slide-description").append("<p>Falls ihr es nicht beim ersten Mal geschafft habt, könnt ihr einfach nochmal neu anfangen" +
            ", indem ihr entweder wieder auf das Feld klickt oder nochmal die Seite refresht! Dann achtet auf die alleinstehenden" +
            " Buchstaben.</p>");
    }

    $(document).keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            $('#submit').click();
            return false;
        }
    });

    $("#hint").click(function () {
        $(".question-hint").fadeToggle(200);
    });

    function flashWindow() {
        $(".question-box").animate({
            left: "+=25"
        }, 50);
        $(".question-box").animate({
            left: "-=25"
        }, 50)
    }



});
