INSERT INTO solutions(id, solution_name, solution_mapping) VALUES (99, 'validName', 'validMapping');

INSERT INTO entries(id, entry_name, entry_mapping) VALUES (99, 'validName', 'validMapping');

INSERT INTO message_board_posts(id, message, link, poster, time_posted)
VALUES (5 , 'message', 'link', 'poster', '2017-05-21 10:00:00');

INSERT INTO message_board_posts(id, message, link, poster, time_posted)
VALUES (10 , 'message', 'link', 'poster', '2017-05-19 10:00:00');

INSERT INTO message_board_posts(id, message, link, poster, time_posted)
VALUES (15 , 'message', 'link', 'poster', '2017-05-19 09:59:00');

INSERT INTO message_board_posts(id, message, link, poster, time_posted)
VALUES (20 , 'message', 'link', 'poster', '2001-04-02 10:00:00');

INSERT INTO wrong_answers(id, answer, origin, time_entered)
VALUES (5 , 'testAnswer1', 'Solution', '2001-04-02 10:00:00');

INSERT INTO wrong_answers(id, answer, origin, time_entered)
VALUES (10 , 'testAnswer2', 'Solution', '2001-04-03 10:00:00');

INSERT INTO wrong_answers(id, answer, origin, time_entered)
VALUES (15 , 'testAnswer3', 'Entry', '2001-04-04 10:00:00');

INSERT INTO wrong_answers(id, answer, origin, time_entered)
VALUES (20 , 'testAnswer4', 'Entry', '2001-04-05 10:00:00');

