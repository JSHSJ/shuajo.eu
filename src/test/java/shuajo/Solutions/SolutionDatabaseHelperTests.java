package shuajo.Solutions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import shuajo.ExceptionHandling.Exceptions.SolutionNotFoundException;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by joshua on 5/20/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class SolutionDatabaseHelperTests {

    @Autowired
    SolutionDatabaseHelper solutionDatabaseHelper;

    private Solution validSolution;
    private final String validSolutionName = "validName";
    private final String validSolutionMapping = "validMapping";



    @Test
    public void testHelperReturnsValidSolutionMappingFromRepository() throws Exception{
        String mapping = this.solutionDatabaseHelper.getSolutionByName(validSolutionName);
        assertTrue(mapping.equals(validSolutionMapping));
    }

    @Test(expected=SolutionNotFoundException.class)
    public void testHelperThrowsExceptionWhenNoValidSolution() {
        String invalidMapping = this.solutionDatabaseHelper.getSolutionByName("invalidSolution");
        assertTrue(invalidMapping == null);
    }



}
