package shuajo.Validators;

import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import shuajo.MessageBoard.MessageBoardPost;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by joshua on 5/20/17.
 */
public class SubmitPostValidatorTests {

    private MessageBoardPost post;
    private final String emptyString = "";
    private final String validMessage = "Test Message";
    private final String whiteSpaceMessage = "     ";
    private final String validPoster = "test";

    @Test
    public void testValidatorWithInvalidClass() {
        SubmitPostValidator submitPostValidator = new SubmitPostValidator();
        String invalidClass = "";
        assertFalse(submitPostValidator.supports(invalidClass.getClass()));
    }

    @Test
    public void testValidatorWithValidClass() {
        SubmitPostValidator submitPostValidator = new SubmitPostValidator();
        MessageBoardPost validClass = new MessageBoardPost();
        assertTrue(submitPostValidator.supports(validClass.getClass()));

    }

    @Test
    public void testValidatorWithValidPost() {
        SubmitPostValidator submitPostValidator = new SubmitPostValidator();
        MessageBoardPost validPost = new MessageBoardPost();
        validPost.setPoster(validPoster);
        validPost.setMessage(validMessage);
        Errors errors = new BeanPropertyBindingResult(validPost, "validPost");
        submitPostValidator.validate(validPost, errors);

        assertFalse(errors.hasErrors());
    }

    @Test
    public void testValidatorWithInvalidPosterName() {
        SubmitPostValidator submitPostValidator = new SubmitPostValidator();
        MessageBoardPost invalidPost = new MessageBoardPost();
        invalidPost.setMessage(validMessage);
        invalidPost.setLink(emptyString);
        invalidPost.setPoster("This is an invalid Poster Name");
        Errors errors = new BeanPropertyBindingResult(invalidPost, "invalidPost");
        submitPostValidator.validate(invalidPost, errors);

        Boolean hasInvalidPosterFieldError = (errors.getFieldError("poster").toString().contains("poster.wrongLength"));
        assertTrue(hasInvalidPosterFieldError);
    }

    @Test
    public void testValidatorWithEmptyPost() {
        SubmitPostValidator submitPostValidator = new SubmitPostValidator();
        MessageBoardPost emptyPost = new MessageBoardPost();
        emptyPost.setMessage(emptyString);
        emptyPost.setLink(emptyString);
        Errors errors = new BeanPropertyBindingResult(emptyPost, "emptyPost");
        submitPostValidator.validate(emptyPost, errors);

        Boolean hasEmptyPostFieldError = (errors.getFieldError("message").toString().contains("message.emptyPost"));
        assertTrue(hasEmptyPostFieldError);
    }
    @Test
    public void testValidatorWithTooLongMessage() {
        String messageTooLong = createTooLongMessage();
        SubmitPostValidator submitPostValidator = new SubmitPostValidator();
        MessageBoardPost tooLongPost = new MessageBoardPost();
        tooLongPost.setMessage(messageTooLong);
        tooLongPost.setLink(emptyString);
        Errors errors = new BeanPropertyBindingResult(tooLongPost, "tooLongPost");
        submitPostValidator.validate(tooLongPost, errors);

        Boolean hasMessageTooLongFieldError = (errors.getFieldError("message").toString().contains("message.tooLong"));
        assertTrue(hasMessageTooLongFieldError);
    }

    private String createTooLongMessage() {
        String tooLong = "";
        for (int i = 0; i < 202; i++) {
            tooLong += "a";
        }
        return tooLong;
    }

    @Test
    public void testValidatorWithMessageContainingOnlyWhitespaces() {
        SubmitPostValidator submitPostValidator = new SubmitPostValidator();
        MessageBoardPost whiteSpacePost = new MessageBoardPost();
        whiteSpacePost.setMessage(whiteSpaceMessage);
        whiteSpacePost.setLink(whiteSpaceMessage);
        Errors errors = new BeanPropertyBindingResult(whiteSpacePost, "whiteSpacePost");
        submitPostValidator.validate(whiteSpacePost, errors);

        Boolean hasEmptyPostFieldError = (errors.getFieldError("message").toString().contains("message.emptyPost"));
        assertTrue(hasEmptyPostFieldError);
    }

}
