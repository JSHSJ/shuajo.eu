package shuajo.Web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by joshua on 5/23/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class EntryControllerTests {


    @Autowired
    MockMvc mockMvc;

    @Test
    public void testGetRequestToEntryReturnsCorrectPage() throws Exception {
        this.mockMvc.perform(get("/entry")
        .accept(MediaType.TEXT_HTML))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("Zugang")));
    }

    @Test
    public void testPostWithValidEntryReturnsCorrectRedirect() throws Exception {
        this.mockMvc.perform(post("/entry")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("entryName" , "validName"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/entry/validMapping"));
    }

    @Test
    public void testPostWithInvalidEntryReturnsErrorPage() throws Exception {
        String invalidName = "invalidName";
        this.mockMvc.perform(post("/entry")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("entryName", invalidName))
                .andExpect(status().is(404))
                .andExpect(content().string(containsString("Entry &quot;" + invalidName + "&quot; could not be found!" )));
    }

    @Test
    public void testDirectAccessToValidEntryWithMapping() throws Exception {
        String validMapping = "validMapping";
        this.mockMvc.perform(get("/entry/"+validMapping))
                .andExpect(status().isOk());
    }
}
