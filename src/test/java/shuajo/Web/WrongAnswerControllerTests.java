package shuajo.Web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import shuajo.ExceptionHandling.Exceptions.EntryNotFoundException;
import shuajo.WrongAnwers.WrongAnswerDatabaseHelper;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by joshua on 5/26/17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class WrongAnswerControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    WrongAnswerDatabaseHelper wrongAnswerDatabaseHelper;

    private final String wrongAnswer = "testAnswer";

    @Test
    public void testWrongAnswerMappingReturnsCorrectWebsiteContainingWrongAnswer() throws Exception {
        EntryNotFoundException e = new EntryNotFoundException(wrongAnswer);
        wrongAnswerDatabaseHelper.createNewWrongAnswerFromException(e);

        mockMvc.perform(get("/wrongAnswers")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Falsche Antworten")))
                .andExpect(content().string(containsString(wrongAnswer)));

    }
}
