package shuajo.Web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import shuajo.MessageBoard.MessageBoardHelper;
import shuajo.MessageBoard.MessageBoardPost;
import shuajo.MessageBoard.MessageBoardPostRepository;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by joshua on 5/23/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WebAppConfiguration
@ActiveProfiles(profiles = "test")
public class MessageBoardControllerTests {


    @Autowired
    MockMvc mockMvc;

    @Autowired
    MessageBoardHelper messageBoardHelper;

    String emptyString = "";
    String validMessage = "test message";
    String validPoster = "test";
    String invalidPoster = "TESTTESTTEST";
    String validLink = "http://shuajo.eu";
    String invalidLink = "this is not a link";

    @Test
    public void testGetRequestToMessageBoardReturnsCorrectPage() throws Exception {
        this.mockMvc.perform(get("/messageBoard")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Message Board")));
    }

    @Test
    public void testPostRequestWithValidPostsReturnsCorrectPageWithPost() throws Exception {
        this.mockMvc.perform(post("/messageBoard")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .param("poster", validPoster)
            .param("message", validMessage)
            .param("link", validLink))
                    .andExpect(status().isOk())
                    .andExpect(content().string(containsString(validMessage)))
                    .andExpect(content().string(containsString(validPoster)));
    }

    @Test
    public void testPostRequestWithInvalidPostsReturnsMessageBoardWithCorrespondingErrorMessage() throws Exception {
        this.mockMvc.perform(post("/messageBoard")
        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
        .param("poster", invalidPoster)
        .param("message", emptyString)
        .param("link", emptyString))
                .andExpect(content().string(containsString("Name must be between 3 and 10 characters long")))
                .andExpect(content().string(containsString("Please enter at least a link or a message")))
                .andExpect(model().hasErrors());
    }

    @Test
    public void testSavingNewPostIsDisplayedOnWebpage() throws Exception{
        MessageBoardPost messageBoardPost = createNewMessageBoardPost();
        savePost(messageBoardPost);

        this.mockMvc.perform(get("/messageBoard"))
                .andExpect(content().string(containsString(messageBoardPost.getMessage())));

    }

    private MessageBoardPost createNewMessageBoardPost() {
        MessageBoardPost messageBoardPost = new MessageBoardPost();
        messageBoardPost.setLink(validLink);
        messageBoardPost.setMessage("super test message");
        messageBoardPost.setPoster(validPoster);
        return messageBoardPost;
    }

    private void savePost(MessageBoardPost post) {
        this.messageBoardHelper.setCurrentPost(post);
        this.messageBoardHelper.saveNewPost();
    }


}
