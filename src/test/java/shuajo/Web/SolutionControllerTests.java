package shuajo.Web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by joshua on 5/24/17.
 * For information on why the used Strings work refer to data.sql in /resources/test
 */


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class SolutionControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void testGetRequestToEntryReturnsCorrectPage() throws Exception {
        this.mockMvc.perform(get("/solution")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Lösung")));
    }

    @Test
    public void testPostRequestWithValidSolutionReturnsCorrectRedirect() throws Exception {
        String validName = "validName";
        String validMapping = "validMapping";
        this.mockMvc.perform(post("/solution")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("solutionName", validName))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/solution/"+validMapping));
    }

    @Test
    public void testPostRequestWithInvalidSolutionReturnsErrorPage() throws Exception {
        String invalidName = "invalidName";
        this.mockMvc.perform(post("/solution")
            .contentType(MediaType.APPLICATION_FORM_URLENCODED)
            .param("solutionName", invalidName))
                .andExpect(status().is(404))
                .andExpect(content().string(containsString("Solution &quot;" + invalidName + "&quot; could not be found!")));
    }

    @Test
    public void testDirectAccessToValidSolutionWithMapping() throws Exception {
        String validMapping = "validMapping";
        this.mockMvc.perform(get("/solution/"+validMapping))
                .andExpect(status().isOk());
    }
}
