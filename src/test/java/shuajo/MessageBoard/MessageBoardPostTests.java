package shuajo.MessageBoard;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by joshua on 5/23/17.
 */

public class MessageBoardPostTests {

    private final String testMessage = "test message";
    private final String testLink = "http://test.link";
    private final String testPoster = "test";
    private final String emptyString = "";

    @Autowired
    MessageBoardPost messageBoardPost;



    @Test
    public void testIfGetContentWithEmptyLinkContainsMessage() throws Exception {
        MessageBoardPost testPost = createFullyFilledPost();
        testPost.setLink(emptyString);
        assertTrue(testPost.getContent().equals(testMessage));
    }

    @Test
    public void testIfGetContentWithLinkAndMessageContainsMessage() throws Exception {
        MessageBoardPost testPost = createFullyFilledPost();
        testPost.setMessage(testMessage);
        testPost.setLink(testLink);
        testPost.setPoster(testPoster);
        assertTrue(testPost.getContent().contains(testMessage));
    }

    @Test
    public void testIfLinkMatchesCorrectLinkFormat() throws Exception {
        MessageBoardPost testPost = createFullyFilledPost();

        String correctLinkFormat ="<a href=\""+this.testLink+"\" target=\"_blank\">"
                +this.testMessage+"</a>";
        assertTrue(testPost.getContent().equals(correctLinkFormat));
    }

    @Test
    public void testIfTimePostedReturnsCorrectTime() throws Exception {
        MessageBoardPost testPost = createFullyFilledPost();
        Date currentTime = getCurrentTime();
        assertTrue(testPost.getTimePosted().equals(currentTime));

    }


    private MessageBoardPost createFullyFilledPost() {
        MessageBoardPost testPost = new MessageBoardPost();
        testPost.setMessage(testMessage);
        testPost.setLink(testLink);
        testPost.setPoster(testPoster);
        testPost.setTimePosted(getCurrentTime());
        return testPost;
    }

    private Date getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        return new Date(cal.getTime().getTime());
    }
}
