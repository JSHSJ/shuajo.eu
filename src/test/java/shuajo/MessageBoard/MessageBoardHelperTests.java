package shuajo.MessageBoard;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import shuajo.Helper.TimeHelper;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by joshua on 5/23/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class MessageBoardHelperTests {

    @Autowired
    MessageBoardHelper messageBoardHelper;

    @Autowired
    TimeHelper timeHelper;

    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.M hh:mm");

    /*
    Set previousDate to now, so all Posts on the list have to be earlier.
    Assert that date on List are ordered.
     */

    @Test
    public void testPostListContainsPostsOrderedByDescendingTime() throws Exception{
        List<MessageBoardPost> allPosts = this.messageBoardHelper.getMessageBoardPosts();

        Date previousDate = getCurrentTime();

        for (MessageBoardPost m : allPosts) {
            Date dateToTest = m.getTimePosted();
            System.out.println(dateToTest);
            assertTrue(dateToTest.before(previousDate));
            previousDate = dateToTest;
        }
    }

    /*
    Size of 4 is chose by SQL statements
    See data.sql for insert commands.
     */

    @Test
    public void testPostListContainsAllPosts() throws Exception {
        List<MessageBoardPost> allPosts = this.messageBoardHelper.getMessageBoardPosts();
        assertTrue(allPosts.size()==4);
    }

    @Test
    public void testNewPostHasCorrectTimestamp() {

        MessageBoardPost testPost = createNewMessageBoardPost();
        this.messageBoardHelper.setCurrentPost(testPost);
        this.messageBoardHelper.saveNewPost();
        Date testDate = getCurrentTime();

        List<MessageBoardPost> allPosts = this.messageBoardHelper.getMessageBoardPosts();
        MessageBoardPost savedPost = allPosts.get(0);
        System.out.println(savedPost.getTimePosted());
        System.out.println(testDate);
        assertTrue(compareDatesFormated(savedPost.getTimePosted(), testDate));
    }

    private boolean compareDatesFormated(Date date1, Date date2) {
        return dateFormat.format(date1).equals(dateFormat.format(date2));
    }

   private Date getCurrentTime() {
        return timeHelper.calculateCurrentTime();
    }

    private MessageBoardPost createNewMessageBoardPost() {
        MessageBoardPost messageBoardPost = new MessageBoardPost();
        messageBoardPost.setLink("http://www.test.link");
        messageBoardPost.setMessage("test message");
        messageBoardPost.setPoster("test");
        return messageBoardPost;
    }


}
