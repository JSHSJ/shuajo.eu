package shuajo.Entries;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import shuajo.ExceptionHandling.Exceptions.EntryNotFoundException;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by joshua on 5/23/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class EntryDatabaseHelperTests {

    @Autowired
    EntryDatabaseHelper entryDatabaseHelper;

    private final String validEntryName = "validName";
    private final String validEntryMapping = "validMapping";



    @Test
    public void testHelperReturnsvalidEntryMappingFromRepository() throws Exception{
        String mapping = this.entryDatabaseHelper.getEntryByName(validEntryName);
        assertTrue(mapping.equals(validEntryMapping));
    }

    @Test(expected=EntryNotFoundException.class)
    public void testHelperThrowsExceptionWhenNoValidSolution() {
        String invalidMapping = this.entryDatabaseHelper.getEntryByName("invalidEntry");
        assertTrue(invalidMapping == null);
    }

}

