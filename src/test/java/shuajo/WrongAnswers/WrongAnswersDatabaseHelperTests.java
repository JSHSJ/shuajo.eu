package shuajo.WrongAnswers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import shuajo.Entries.Entry;
import shuajo.ExceptionHandling.Exceptions.EntryNotFoundException;
import shuajo.Helper.TimeHelper;
import shuajo.WrongAnwers.WrongAnswer;
import shuajo.WrongAnwers.WrongAnswerDatabaseHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by joshua on 5/25/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class WrongAnswersDatabaseHelperTests {

    @Autowired
    WrongAnswerDatabaseHelper wrongAnswerDatabaseHelper;

    @Autowired
    TimeHelper timeHelper;

    private String testAnswer = "test Answer";

    @Test
    public void assertEntryNotFoundExceptionParsingCreatesCorrectWrongAnswer() throws Exception {
        EntryNotFoundException entryNotFoundException = new EntryNotFoundException(testAnswer);
        this.wrongAnswerDatabaseHelper.createNewWrongAnswerFromException(entryNotFoundException);
        WrongAnswer wrongAnswer = this.wrongAnswerDatabaseHelper.getWrongEntries().get(0);
        System.out.println(wrongAnswer.getAnswer());
        assertTrue(wrongAnswer.getAnswer().equals(testAnswer));
    }

    @Test
    public void assertGetAllAnswersReturnsAllAnswersOrderedCorrectly() throws Exception{
        List<WrongAnswer> allAnswers = this.wrongAnswerDatabaseHelper.getAllWrongAnswers();
        assert(allAnswers.size()==4);
        splitListIntoPartsAndAssertOrderedByDate(allAnswers);
    }

    private void splitListIntoPartsAndAssertOrderedByDate(List<WrongAnswer> allAnswers) {
        ArrayList<WrongAnswer> entries = new ArrayList<>();
        ArrayList<WrongAnswer> solutions = new ArrayList<>();
        for (WrongAnswer a : allAnswers) {
            System.out.println(a.getAnswer());
            if (a.getOrigin().equals("Entry")) {
                entries.add(a);
            } else {
                solutions.add(a);
            }
        }
        assertListIsOrderedByDate(entries);
        assertListIsOrderedByDate(solutions);
    }

    private void assertListIsOrderedByDate(ArrayList<WrongAnswer> list) {
        Date testDate = timeHelper.calculateCurrentTime();
        for (WrongAnswer answer : list) {
            assert(answer.getTimeEntered().before(testDate));
            testDate = answer.getTimeEntered();
        }
    }

    @Test
    public void assertAnswersAsNamesByOriginReturnsListOfCorrectStrings() throws Exception {
        List<String> answersAsString = this.wrongAnswerDatabaseHelper.getWrongAnswerNamesByOrigin("Entry");
        for (String s : answersAsString) {
            assertTrue(s instanceof String);
            System.out.println(s);
            assertTrue(s.contains("test Answer") || s.contains("testAnswer"));
        }
    }
}
